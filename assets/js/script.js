var app = angular.module('socialSearch',['ngRoute']);

app.config(function($locationProvider){
    $locationProvider.html5Mode({enabled:true, requireBase: false});
})
app.controller("searchCtrl",function($scope, $http){
    $scope.search = function(query){
        $http.get('search?email='+query)
            .success(function(user){
                $scope.data = user;
            })
    }
});

app.controller("adminCtrl",function($scope, $http){
    $scope.getAll = function(){
        $http.get('all')
            .success(function(user){
                $scope.data = user
            })
    }
    $scope.remove = function(idx){
        var user_to_delete = $scope.data[idx]
        $http.get('remove?email='+user_to_delete.email)
            .success(function(user){
                $scope.data.splice(idx, 1)
            })
    }
})

app.controller("queryCtrl",function($rootScope,$scope){
    $scope.getParam = function(data){
        $scope.data = data;
    }
})


