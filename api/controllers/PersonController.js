var request = require("request")
var apiKey = '959845189c2b8850';
module.exports = {
    save : function(req, res){
        var email = req.param('email');
        request.get('https://api.fullcontact.com/v2/person.json?email='+email+'&style=dictionary&apiKey='+apiKey, function(err, resp, body){
            var data = JSON.parse(body)
                Person.create(data).exec(function(err, personExec){
                    personExec.email = email;
                    personExec.save();
                    res.json(data);
                });

        })
    },
    liveSearch: function(req, res){
        var email = req.param('email');

        request.get('https://api.fullcontact.com/v2/person.json?email='+email+'&style=dictionary&apiKey='+apiKey, function(err, resp, body){
            var data = JSON.parse(body)
            if(data.status == 200){
                Person.find({email:email}).exec(function(err, personExec){
                    if (personExec.length === 0 || typeof personExec === 'undefined'){
                        Person.create(data).exec(function(err, newData){
                            newData.email = email;
                            newData.save();
                            var myArray = [];
                            myArray.push(newData)
                            res.json(myArray);
                        });
                    }else{
                        res.json(personExec)
                    }
                })
            }else{
                return res.json([])
            }


        })

    },
    getAll: function(req, res){
        Person.find().exec(function(err, personExec){
            res.json(personExec);
        })
    },
    delete: function(req, res){
        var email = req.param('email');
        Person.destroy({email:email}).exec(function(err, personExec){
            res.send(200);
        })
    },
    querySearch: function(req, res){
        var email = req.param('email');

        request.get('https://api.fullcontact.com/v2/person.json?email='+email+'&style=dictionary&apiKey='+apiKey, function(err, resp, body){
            var data = JSON.parse(body)
            if(data.status == 200){
                Person.find({email:email}).exec(function(err, personExec){
                    if (personExec.length === 0 || typeof personExec === 'undefined'){
                        Person.create(data).exec(function(err, newData){
                            newData.email = email;
                            newData.save();
                            var myArray = [];
                            myArray.push(newData)
                            res.render('query',{data:myArray})
                        });
                    }else{
                        res.render('query',{data:personExec})
                    }
                })
            }else{
                return res.render('query')
            }


        })

    }


    }